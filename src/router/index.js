import Vue from 'vue'
import Router from 'vue-router'
import audit from '@/components/audit'
import login from '@/components/login'
import axios from 'axios'


const SERVER = '172.19.0.14'
const PORT = '8081'
const PROTOCOL = 'http'

Vue.use(Router)

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

const token = getCookie('token')

async function verify_jwt(){
  var respon = ''

  if ( token.length != 0) {
      try {
      const my_verify = await axios({
          url: `${PROTOCOL}://${SERVER}:${PORT}/verify`,
          headers: {
            'jwt': token,
            'Content-Type': 'application/json'
          },
          method: 'post'
      })

      return my_verify.status

    }catch (e) {
       return 403
    }
  }else {

    return 403
  }

}


export const router =  new Router({
  mode: 'history',
  routes: [
    {
      path: '/audit',
      name: 'audit',
      component: audit,

    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/',
      redirect: '/login'
    }
  ]
})

// Funcion para obtener el jwt de la cookie


router.beforeEach((to, from, next) => {

  (async function () {
     const token_verified = await verify_jwt()
     if ( to.path == '/login' && token_verified == 403) {
       next()
     }else if (to.path != '/login' && token_verified == 403) {
         next('/login')
     }else if (to.path != '/login' && token_verified == 200){
        next()
     }else if (to.path == '/login' && token_verified == 200){
        next('/audit')
     }
  })()
})

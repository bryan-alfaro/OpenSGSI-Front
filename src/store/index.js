import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
const SERVER = '127.0.0.1'
const PORT = '5000'
const PROTOCOL = 'http'



export const store = new Vuex.Store({
	state: {
		projects : [],  
		controls : [],
		policies: [],
		selected_project : null,
		selected_control : null,
		front: {
          'drawer_main': true
		},
		// Creando proyecto
		postResquestMode: 'modeForm',
		postResquestResp: '',
		/* controls version ultimate*/
		normative_controls: [],
		normative_domain: [],
		index_id_select: null,
		index_controlObjectSelect: null
	},
	mutations: {

		editPostRequestResp (state, data) {
			state.postResquestResp = data
		},
		editPostRequestMode (state, data) {
			state.postResquestMode = data
		},
        projects ( state, data ) {
        	state.projects.length = 0
        	for (let project of data ) {
        		state.projects.push(project)
            }           
        },
        edit_sel_project ( state, id_pro ){
        	state.selected_project = id_pro
        	localStorage.setItem("selected_project",id_pro)
        },

        control_for_proj ( state, data ) {        	
        	//let control_list = []
            state.controls.length = 0
        	for (let first_control  of data ) {
        		state.controls.push(first_control)
        	}         	
        },

        edit_sel_control (state, id_cont) {        	
        	state.selected_control = id_cont
        	localStorage.setItem("selected_control",id_cont)
        },
        policy_for_control (state, data) {        	
        	state.policies.length = 0
        	for (let policy  of data ) {
        		state.policies.push(policy)
        	} 
        },
        set_normative (state, payload) {
        	state.normative_controls.length = 0
        	state.normative_domain.length = 0

            for (let normative of payload) {
                 state.normative_controls.push(normative)
                 for (let control of normative.controls){
                 	state.normative_domain.push(control)
                 }         
            } 

            
        },
        select_domain (state, index) {
        	state.index_id_select = index
        	localStorage.setItem("selected_domain", index)
        },
        select_domainControl (state, index) {
            state.index_controlObjectSelect = index
        	localStorage.setItem("selected_controlObject", index)
        }
 
	},	
	actions: {
		post_create_project (context, payload) {

		    console.log(payload)
            axios.post(`${PROTOCOL}://${SERVER}:${PORT}/project`,payload
            )
            .then( (resp) => {
            	context.commit('editPostRequestResp', resp.data.msg)
            	context.commit('editPostRequestMode', 'modeResult')                               
            })
            .catch( (err) => {
            	context.commit('editPostRequestResp', 'Error Server')
            	context.commit('editPostRequestMode',  'modeResult')
            })
		},

		projects ( context ) {   

            axios.get(`${PROTOCOL}://${SERVER}:${PORT}/project`)
            .then( (resp) => {
                context.commit('projects', resp.data)                
            })
            .catch( (err) => console.log(err))
		},

		controls (context, idproject) {

			axios.get(`${PROTOCOL}://${SERVER}:${PORT}/control?idproject=${idproject}`)
			.then( (resp) => {				
                context.commit('control_for_proj', resp.data)

			}).catch( (err) => console.log(err))
		},

		policies ( context, idcontrol ) {

			axios.get(`${PROTOCOL}://${SERVER}:${PORT}/policy?idcontrol=${idcontrol}`)
			.then( (resp) => {				
                context.commit('policy_for_control', resp.data)

			}).catch( (err) => console.log(err))
		},
		Normatives (context ) {
			axios.get(`${PROTOCOL}://${SERVER}:${PORT}/normative`) 
	        .then( (resp) => {
	            context.commit('set_normative', resp.data)
	        }).catch( (err) => {
	            console.log(err)
	        })
		}
	},
	getters: {

		drawer_main ( state )  {
          return state.front.drawer_main
		},

		get_projects ( state ) {
			return state.projects
		},

		get_sel_project (state) { 
			if (state.selected_project != null) {
				return state.selected_project
			}else if( localStorage.getItem("selected_project") != null ) {
                return localStorage.getItem("selected_project")
			}        
			
		},

		get_controls_proj ( state ) {

			function num(a) {
	          var b = a.Control_num.split(".")
	          return b[0] * 10000 + b[1] * 100 + b[2];
	        }
	        let mycontrols = state.controls
			mycontrols.sort( (a, b) => {
				return num(a) - num(b)
			})
			return mycontrols
		},

		get_sel_control( state ) {

            if (state.selected_control != null) {
				return state.selected_control
			}else if( localStorage.getItem("selected_control") != null ) {
                return localStorage.getItem("selected_control")
			}
            
		},

		get_policies_control (state) {
			return state.policies
		},

		getPost_projMode (state) {
			return state.postResquestMode
		},

		getPost_projResp (state) {
			return state.postResquestResp
		},

		getNormativeDomain (state) {
			return state.normative_domain
		},

		getNormative (state) {
			return state.normative_controls
		},

		getIndexDomain( state ) {

			if (state.index_id_select != null) {
				return state.index_id_select
			}else if( localStorage.getItem("selected_domain") != null ) {
                return localStorage.getItem("selected_domain")
			}
			
		},
		getIndexObjectControl( state ) {

			if (state.index_controlObjectSelect != null) {
				return state.index_controlObjectSelect
			}else if( localStorage.getItem("selected_controlObject") != null ) {
                return localStorage.getItem("selected_controlObject")
			}
			
		}



	},
})


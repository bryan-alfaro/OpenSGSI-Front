// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import { router }  from './router'
import Vuetify from 'vuetify'
import { sync } from 'vuex-router-sync'
import axios from 'axios'
import 'vuetify/dist/vuetify.min.css'


const SERVER = '127.0.0.1'
const PORT = '3000'
const PROTOCOL = 'http'

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function verifyToken(t) {

  axios({
    url: `${PROTOCOL}://${SERVER}:${PORT}/verify`,
    headers: {
      'jwt': t,
      'Content-Type': 'application/json'
    },
    method: 'post'
  }).then( (resp) => {

      return resp

  }).catch((err) => console.log(err))
}

Vue.use(Vuetify, { theme: {
  primary: '#009688',
  secondary: '#424242',
  accent: '#82B1FF',
  error: '#FF5252',
  info: '#2196F3',
  success: '#4CAF50',
  warning: '#FFC107'
}})

Vue.config.productionTip = false

/* eslint-disable no-new */

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
